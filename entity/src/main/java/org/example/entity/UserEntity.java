package org.example.entity;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class UserEntity {

    @Id
    private Long id;
    private String firstName;
    private String lasttName;
    private String email;

}
